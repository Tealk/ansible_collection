# check_version Rolle

Diese Rolle überprüft die installierte Version einer Software gegen die neueste verfügbare Version auf GitHub und setzt eine Variable, wenn ein Upgrade erforderlich ist.

## Variablen

- `github_repo`: Das GitHub-Repository im Format `user/repo`
- `version_file_path`: Der Pfad zur Datei, die die installierte Version enthält

## Beispiel-Playbook

```yaml
- hosts: all
  roles:
    - role: check_version
      vars:
        github_repo: "user/andere_software"
        version_file_path: "/opt/andere_software/version"
```
